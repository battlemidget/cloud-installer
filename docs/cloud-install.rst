
.. code::

   cloud-install [-ih]

   Create an Ubuntu Cloud! (requires root privileges)

   Options:
     -i    install only (don't invoke cloud-status)
     -h    print this message


DESCRIPTION
===========

Ubuntu OpenStack Installer provides an extremely simple way to
install, deploy and scale an OpenStack cloud on top of Ubuntu server
and Juju. Deploy onto a single physical system using virtual machines
for testing, or use MAAS to deploy onto a cluster of any size.
